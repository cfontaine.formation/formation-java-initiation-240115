/**
 * Classe Helloworld
 * 
 * @author moi
 */
public class HelloWorld {
    
    /**
     * Le point d'entrée du programme
     * @param args argument ligne de commande
     */
    public static void main(String[] args) { // Commentaire fin ligne
        
        /*
         * Commentaire 
         * sur 
         * plusieurs 
         * lignes
         */
        System.out.println("Hello World!!!");

    }

}
