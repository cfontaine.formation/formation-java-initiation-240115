package fr.dawan.formation;

public abstract class Forme {

    private Couleur couleur;

    public Forme(Couleur couleur) {
        this.couleur = couleur;
    }

    public Couleur getCouleur() {
        return couleur;
    }

    public void setCouleur(Couleur couleur) {
        this.couleur = couleur;
    }

    public abstract double calculSurface();

    @Override
    public String toString() {
        return "Couleur=" + couleur;
    }

}
