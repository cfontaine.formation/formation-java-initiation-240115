package fr.dawan.formation;

public class Application {

    public static void main(String[] args) {
        Terrain t = new Terrain();
        t.ajouter(new Rectangle(1.0, 1.0, Couleur.BLEU));
        t.ajouter(new Rectangle(1.0, 1.0, Couleur.BLEU));
        t.ajouter(new Cercle(1.0, Couleur.ROUGE));
        t.ajouter(new Cercle(1.0, Couleur.ROUGE));
        t.ajouter(new TriangleRectangle(1.0, 1.0, Couleur.VERT));
        t.ajouter(new TriangleRectangle(1.0, 1.0, Couleur.VERT));
        t.ajouter(new Rectangle(1.0, 1.0, Couleur.ORANGE));
        t.ajouter(new TriangleRectangle(1.0, 1.0, Couleur.ORANGE));
      
        System.out.println("Surface=" + t.surface());
        System.out.println("Surface Rouge=" + t.surface(Couleur.ROUGE));
        System.out.println("Surface Bleu=" + t.surface(Couleur.BLEU));
        System.out.println("Surface Vert=" + t.surface(Couleur.VERT));
        System.out.println("Surface Orange=" + t.surface(Couleur.ORANGE));
    }

}
