package fr.dawan.formation;

public class TriangleRectangle extends Rectangle {

    public TriangleRectangle(double largeur, double longueur, Couleur couleur) {
        super(largeur, longueur, couleur);
    }

    @Override
    public double calculSurface() {
        return super.calculSurface() / 2.0;
    }

    @Override
    public String toString() {
        return "Triangle" + super.toString();
    }

}
