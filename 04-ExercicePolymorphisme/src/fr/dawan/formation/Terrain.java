package fr.dawan.formation;

public class Terrain {

    private Forme[] formes = new Forme[10];

    private int nbForme;

    public void ajouter(Forme forme) {
        if (nbForme < formes.length) {
            formes[nbForme] = forme;
            nbForme++;
        }
    }

    public double surface() {
        double somme = 0.0;
        for (int i = 0; i < nbForme; i++) {
            somme += formes[i].calculSurface();
        }
        return somme;
    }

    public double surface(Couleur couleur) {
        double somme = 0.0;
        for (int i = 0; i < nbForme; i++) {
            if (formes[i].getCouleur() == couleur) {
                somme += formes[i].calculSurface();
            }
        }
        return somme;
    }
}
