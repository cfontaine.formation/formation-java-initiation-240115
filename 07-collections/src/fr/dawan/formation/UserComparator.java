package fr.dawan.formation;

import java.util.Comparator;

public class UserComparator implements Comparator<User> {

    @Override
    public int compare(User o1, User o2) {
        int cmp=o1.getPrenom().compareTo(o2.getPrenom());
        if(cmp==0) {
            if(o1.getAge()==o2.getAge()) {
                return 0;
            }
            else {
                cmp=o1.getAge()>o2.getAge()?1:-1;
            }
        }
        return cmp;
    }

}
