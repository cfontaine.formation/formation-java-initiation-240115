package fr.dawan.formation;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Queue;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import fr.dawan.formation.beans.Employe;
import fr.dawan.formation.beans.Entreprise;
import fr.dawan.formation.beans.TypeContrat;

@SuppressWarnings("rawtypes")
public class Main {

    @SuppressWarnings("unchecked")
    public static <T> void main(String[] args) {
        // Avant java SE 5 =>   on pouvait stocker tous les objets qui héritent de Object
        //                      dans une collection

        List list1 = new ArrayList();
        list1.add("hello");
        list1.add(12); // autoboxing convertion automatique type primtif -> objet de type enveloppe
                       // implicitement new Integer(12)
        list1.add(3.14);

        // get => récupérer un objet stocké dans la liste à l'indice 0 retourne des objects,
        // il faut tester si l'objet correspont à la classe et le caster
        Object obj = list1.get(0);
        if (obj instanceof String str) {
            // String str =(String) obj;
            System.out.println(str);
        }

        // parcourir avec un itérateur
        Iterator it = list1.iterator();
        while (it.hasNext()) {
            System.out.println(it.next());
        }

        // Exemple classe générique
        Box<String> box1 = new Box<>("Hello");
        System.out.println(box1.getValeur());
        box1.setValeur("world");
        System.out.println(box1.getValeur());

        Box<Integer> box2 = new Box<>(12);
        System.out.println(box2.getValeur());

        // à partir de java 5 => les collections utilisent les types générique
        List<String> lstStr = new ArrayList<>();
        lstStr.add("Hello");
        // lstStr.add(12);
        lstStr.add("World");
        lstStr.add("Azerty");
        lstStr.add("asupprimer");
        lstStr.add("Bonjour");

        // Parcourir une collection "foreach" (java5)
        for (var e : lstStr) {
            System.out.println(e);
        }

        // size -> le nombre d'élément de la collection
        System.out.println(lstStr.size());

        // pour toutes les collecton on peut aussi supprimer un élément de la liste
        // en le passant en paramètre à remove
        System.out.println(lstStr.remove("asupprimer"));

        // On peut afficher le contenu d'un collection avec System.out.println()
        System.out.println(lstStr);

        // ---------------------------------
        // Uniquement pour les List -> index

        // get => renvoie l'élément placé à l'index 1
        System.out.println(lstStr.get(1));

        // set => remplace l'élément à l'index 1 par celui passé en paramètre
        System.out.println(lstStr.set(1, "Monde")); // set -> retourne World
        System.out.println(lstStr);

        // Suppprimer l'élement à l'index 2
        System.out.println(lstStr.remove(2));

        // ---------------------------------
        // Set -> pas de doublon

        Set<Integer> st1 = new HashSet<>();
        st1.add(4);
        st1.add(6);
        System.out.println(st1.add(34)); // 34 est ajouté, add retourne true
        System.out.println(st1.add(34)); // doublons -> 34 existe déjà dans la liste ,
                                         // il n'est pas ajouté et add retourne false
        System.out.println(st1);

        // 34 -> la valeur est supprimée
        st1.remove(34);

        System.out.println(st1);

        // contains -> retourne true, si 6 est présent dans la collection
        System.out.println(st1.contains(6));

        // SortedSet => tous les objets sont automatiquement triés, lorsqu'ils sont
        // ajoutés
        SortedSet<Double> st2 = new TreeSet<>();
        st2.add(4.78);
        st2.add(-14.3);
        st2.add(6.78);
        System.out.println(st2);

        // - Interface Comparable
        // La classe (User) doit implémenter l'interface Comparable pour pouvoir 
        // faire les comparaisons entre objet
        SortedSet<User> stUser = new TreeSet<>();
        stUser.add(new User("John", "Doe", 45));
        stUser.add(new User("Jane", "Doe", 25));
        stUser.add(new User("Alan", "Smithee", 55));
        stUser.add(new User("Yves", "Roulo", 75));
        stUser.add(new User("John", "Doe", 34));
        System.out.println(stUser);

        // - Interface Comparator
        // Un Comparator est externe à la classe (user) dont on veut comparer les
        // éléments
        // On peut créer plusieurs classes implémentant Comparator pour comparer de
        // manière différente les objets ( différent: attributs, ordre de trie)

        // L'objet Comparator est passé au constructeur de TreeSet
        // * On peut utiliser une classe qui impléménte L'interface Comparator
        SortedSet<User> stUser2 = new TreeSet<>(new UserComparator());
        stUser2.add(new User("John", "Doe", 45));
        stUser2.add(new User("Jane", "Doe", 25));
        stUser2.add(new User("Alan", "Smithee", 55));
        stUser2.add(new User("Yves", "Roulo", 75));
        stUser2.add(new User("John", "Doe", 34));
        System.out.println(stUser2);

        // * On peut aussi utiliser une Classe Annonyme ou (une expression Lambda)
        // pour définir le "Comparator"
        SortedSet<User> stUser3 = new TreeSet<>(new Comparator<User>() {

            @Override
            public int compare(User o1, User o2) {
                return o1.getPrenom().compareTo(o2.getPrenom());
            }
        });
        stUser2.add(new User("John", "Doe", 45));
        stUser2.add(new User("Jane", "Doe", 25));
        stUser2.add(new User("Alan", "Smithee", 55));
        stUser2.add(new User("Yves", "Roulo", 75));
        stUser2.add(new User("John", "Doe", 34));
        System.out.println(stUser3);

        // Queue -> file d'attente
        Queue<Integer> fifo = new LinkedList<>();
        fifo.add(4);
        fifo.add(32);
        fifo.add(1);
        fifo.add(8);

        System.out.println(fifo.size());

        // peek => obtenir l'élément disponible, sans le retirer de la file d'attente
        System.out.println(fifo.peek()); // 4
        System.out.println(fifo.peek()); // 4
        System.out.println(fifo);

        // pool => obtenir l'élément disponible et le retirer de la file d'attente
        System.out.println(fifo.poll()); // 4
        System.out.println(fifo.poll()); // 32
        System.out.println(fifo);

        // Map => association Clé/Valeur
        Map<String, User> m = new HashMap<>();
        // put => ajouter la clé "us-1234" et la valeur associé (objet User)
        m.put("us-1234", new User("John", "Doe", 45));
        m.put("us-93", new User("Jane", "Doe", 55));
        m.put("us-75", new User("Alan", "Smithee", 75));

        System.out.println(m.size()); // 3

        // get -> Obtenir la valeur associé à la clé us-0023
        System.out.println(m.get("us-93"));

        // containsKey => test si la clé fait partie de la map la comparaisson se fait avec equals
        System.out.println(m.containsKey("us-1234")); // true

        // containsValue -> test si la valeur fait partie de la map
        System.out.println(m.containsValue(new User("Jane", "Doe", 45))); // false

        // Si la valeur existe déjà, elle est écrasée par la nouvelle valeur associé à la clé
        m.put("us-1234", new User("Marcel", "Dupont", 34));
        System.out.println(m);

        // remove -> supprime la clé et la valeur associée de la map
        m.remove("us-75");

        // keySet => retourne un Set qui contient toutes les clés de la map
        Set<String> keys = m.keySet();
        System.out.println(keys);

        // values => retourne une collection qui contient toutes les valeurs de la map
        Collection<User> users = m.values();
        System.out.println(users);

        // Entry => objet qui contient une clé et la valeur associée
        Set<Entry<String, User>> entry = m.entrySet();
        for (Entry<String, User> e : entry) {
            System.out.println(e.getKey() + " " + e.getValue());
        }

        // isEmpty => test si la collection est vide
        System.out.println(m.isEmpty()); // false

        // clear -> supprime tous les éléments de la collection
        m.clear();
        System.out.println(m.isEmpty()); // true

        // Classe Collections -> Classe utilitaires pour les collections
        Collections.sort(lstStr); // sort -> trie
        System.out.println(lstStr);

        System.out.println(Collections.max(lstStr)); // max -> obtenir la valeur maximum d'une collection

        Collections.shuffle(lstStr); // Mélange aléatoire des éléments de la collection

        System.out.println(lstStr);

        // Classe Arrays -> Classe utilitaire pour les tableaux
        String[] tabStr = new String[5];
        Arrays.fill(tabStr, "Bonjour"); // => initialiser un tableau avec une valeur
        Arrays.toString(tabStr);

        int t1[] = { -5, 7, 9, 0, 1 };
        int t2[] = { -5, 7, 9, 0, 1 };
        System.out.println(Arrays.equals(t1, t2)); // equals => comparaison de deux tableaux
        Arrays.sort(t1); // sort => tri d'un tableau
        Arrays.toString(t1); // toString => afficher un tableau

        // binarySearch => recherche d'un élément dans un tableau trié
        System.out.println(Arrays.binarySearch(t1, 9));
        System.out.println(Arrays.binarySearch(t1, -5));
        int v = Arrays.binarySearch(t1, 8);
        System.out.println(v); // (-5 + 1)*-1 -> 4
        System.out.println(-(v + 1)); // s'il était présent dans le tableau il serait à index 4

        // Exercice Entreprise
        Entreprise en = new Entreprise("Acme");
        en.ajouter(new Employe("John", "Doe", TypeContrat.CDI, LocalDate.of(1997, 12, 9), 2500.0));
        en.ajouter(new Employe("Jane", "Doe", TypeContrat.CDI, LocalDate.of(1999, 1, 19), 2900.0));
        en.ajouter(new Employe("Alan", "Smithee", TypeContrat.CDD, LocalDate.of(1981, 6, 12), 3000.0));
        en.ajouter(new Employe("Jo", "Dalton", TypeContrat.INTERIM, LocalDate.of(1989, 3, 14), 1800.0));
        System.out.println("nombre d'employé=" + en.getNombreEmploye());
        System.out.println("total salaire=" + en.totalSalaireMensuel());
        System.out.println("Salaire moyen=" + en.salaireMoyen());

    }

}
