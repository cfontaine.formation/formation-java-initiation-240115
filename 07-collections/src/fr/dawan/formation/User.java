package fr.dawan.formation;

public class User implements Comparable<User> {
    
    private String prenom;
    
    private String nom;
    
    private int age;

    public User(String prenom, String nom, int age) {
        this.prenom = prenom;
        this.nom = nom;
        this.age = age;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "User [prenom=" + prenom + ", nom=" + nom + ", age=" + age + "]";
    }

    @Override
    public int compareTo(User o) {
       int cmp =nom.compareTo(o.nom);
       if(cmp==0) {
           cmp=prenom.compareTo(o.prenom);
           if(cmp==0) {
               if(age>o.age) {
                   return 1;
               }
               else if(age==o.age) {
                   return 0;
               }
               else {
                   return -1;
               }
           }
       }
        return cmp;
    }
    
    

}
