package fr.dawan.formation;

public class Box<T> {

    private T valeur;

    public Box(T valeur) {
        this.valeur = valeur;
    }

    public T getValeur() {
        return valeur;
    }

    public void setValeur(T valeur) {
        this.valeur = valeur;
    }

}
