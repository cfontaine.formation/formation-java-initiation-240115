package fr.dawan.formation.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

//Pour qu'une classe soit sérialisable, elle doit implémenter l'interface Serializable
public class Entreprise implements Serializable {

    // serialVersionUID est une clé de hachage SHA qui identifie de manière unique la classe.
    // Si la classe personne évolue et n'est plus compatible avec les objets
    // précédamment persistés, on modifie la valeur du serialVersionUID et lors de
    // la désérialisation une exception sera générée pour signaler l'incompatibilité
    // java.io.InvalidClassException: fr.dawan.formation.beans.Personne; local class
    // incompatible: stream classdesc serialVersionUID = 1, local class serialVersionUID = 2
    // si l'on fournit pas serialVersionUID le compilateur va en générer un (à éviter).
    private static final long serialVersionUID = 1L;

    private String nom;

    private List<Employe> employes = new ArrayList<>();

    // Si un attribut ne doit pas être sérialiser on ajout le mot clef transient
    private transient String leCodeDuCoffre;

    private static int cptEmploye; // les variable de classe ne sont pas sérialisée

    public Entreprise(String nom) {
        this.nom = nom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public List<Employe> getEmployes() {
        return employes;
    }

    public static int getCptEmploye() {
        return cptEmploye;
    }

    public static void setCptEmploye(int cptEmploye) {
        Entreprise.cptEmploye = cptEmploye;
    }

    public void setEmployes(List<Employe> employes) {
        this.employes = employes;
    }

    public String getLeCodeDuCoffre() {
        return leCodeDuCoffre;
    }

    public void setLeCodeDuCoffre(String leCodeDuCoffre) {
        this.leCodeDuCoffre = leCodeDuCoffre;
    }

    @Override
    public String toString() {
        return "Entreprise [nom=" + nom + ", employes=" + employes + "]";
    }

}
