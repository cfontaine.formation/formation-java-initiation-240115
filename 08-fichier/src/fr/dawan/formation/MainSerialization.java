package fr.dawan.formation;

import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.time.LocalDate;

import fr.dawan.formation.beans.Employe;
import fr.dawan.formation.beans.Entreprise;
import fr.dawan.formation.beans.TypeContrat;

public class MainSerialization {

    public static void main(String[] args) {
        Entreprise en = new Entreprise("ACME");
        en.setLeCodeDuCoffre("123456");
        Entreprise.setCptEmploye(4);
        en.getEmployes().add(new Employe("John", "Doe", TypeContrat.CDI, LocalDate.of(1997, 12, 9), 2000.0));
        en.getEmployes().add(new Employe("Jane", "Doe", TypeContrat.CDI, LocalDate.of(1994, 1, 19), 2900.0));
        en.getEmployes().add(new Employe("Jo", "Dalton", TypeContrat.CDI, LocalDate.of(1986, 11, 5), 3000.0));
        en.getEmployes().add(new Employe("Alan", "Smithee", TypeContrat.CDD, LocalDate.of(1989, 7, 4), 1800.0));
        serialization(en, "entreprise.bin");
        //
        Entreprise enD = deSerialization("entreprise.bin");
        System.out.println(enD.getNom());
        enD.getEmployes().forEach(System.out::println);
        System.out.println(enD.getLeCodeDuCoffre());
        System.out.println(Entreprise.getCptEmploye());
    }

    // Sérialisation
    // l'objet Entreprise va être sérialiser avec tous les objets qu'il contient
    public static void serialization(Entreprise e, String path) {
        // ObjectOutputStream permet de persiter un objet ou une grappe d'objet
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(path))) {
            oos.writeObject(e);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    // Désérialisation

    public static Entreprise deSerialization(String path) {
        // ObjectInputStream permet de désérialiser
        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(path))) {
            Object obj = ois.readObject();
            if (obj instanceof Entreprise)
                return (Entreprise) obj;
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    // Sérialisation XML
    // Pour pouvoir sérializer un objet en xml avec XMLEncoder,
    // il doit avoir un constructeur par défaut
    // /!\ transcient ne fonction ne pas avec la sérialisation xml
    // -> il faut utiliser la reflexion
    public static void serialisationXML(Entreprise ent, String chemin) {
        try (XMLEncoder os = new XMLEncoder(new FileOutputStream(chemin))) {
            os.writeObject(ent);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // Désérialisation XML => XMLDecoder
    public static Entreprise deSerialisationXML(String chemin) {
        try (XMLDecoder is = new XMLDecoder(new FileInputStream(chemin))) {
            Object obj = is.readObject();
            if (obj instanceof Entreprise) {
                return (Entreprise) obj;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
