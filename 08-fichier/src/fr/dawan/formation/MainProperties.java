package fr.dawan.formation;

import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;
import java.util.Set;

public class MainProperties {

    public static void main(String[] args) {
        // Ecriture des propriétées

        // Properties représente un ensemble persistant de propriétés (clé=valeur)
        // uniquement des chaines de caractères
        Properties prop = new Properties();
        prop.setProperty("version", "1.0");
        prop.setProperty("user", "john doe");
        prop.setProperty("asupprimer", "aaaa");

        prop.remove("asupprimer");

        try {
            // On peut sauver les propriétés :
            // - dans un fichier .properties
            prop.store(new FileWriter("testProperties.properties"), "test ecriture fichier properties");

            // - dans un fichier .xml
            prop.storeToXML(new FileOutputStream("testProperties.xml"), "test ecriture fichier properties");
        } catch (IOException e) {
            e.printStackTrace();
        }

        prop.clear(); // effacer toutes les propriétés

        // Lecture des propriétées
        try {
            // fichier.properties
            prop.load(new FileReader("testProperties.properties"));
            //ou
            // fichier .xml
            // prop.loadFromXML(new FileInputStream("testProperties.xml"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Récupérer la valeur à partir de la clé
        System.out.println(prop.getProperty("version"));
        
        // si la clé n'existe pas => null
        System.out.println(prop.getProperty("existepas")); 
        
        // si la clé n'existe pas => valeur par défaut
        System.out.println(prop.getProperty("existepas", "default value"));
        
        // Récupérer un set contenant toutes les clés
        Set<Object> keys = prop.keySet();
        keys.forEach(System.out::println);
    }

}
