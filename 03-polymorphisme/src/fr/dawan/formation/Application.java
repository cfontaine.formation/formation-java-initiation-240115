package fr.dawan.formation;
import java.time.LocalDate;

import fr.dawan.formation.enums.Motorisation;
import fr.dawan.formation.polymorphisme.Animal;
import fr.dawan.formation.polymorphisme.Canard;
import fr.dawan.formation.polymorphisme.Chat;
import fr.dawan.formation.polymorphisme.Chien;
import fr.dawan.formation.polymorphisme.Refuge;
import fr.dawan.formation.polymorphisme.interfaces.PeutMarcher;
import fr.dawan.formation.records.Personne;

public class Application {

    public static void main(String[] args) {

//        Animal a1 = new Animal(4000, 3); // la classe est abtraite -> on ne peut plus l'instancié
//        a1.emettreSon();

        Chien ch1 = new Chien(5, 3000, "Laika");
        ch1.emettreSon();

        // Polymorphisme
        // L'objet Chien sera vu comme un Animal, on ne pourra pas accèder aux méthodes propre au chien (nom,...)
        // On peut créer une instance de Chien (classe fille) qui aura une référence Animal (classe mère)
        // Upcasting: enfant -> parent Implicite
        Animal a2 = new Chien(5, 7000, "Rolo"); 
        
        System.out.println(a2.getAge() + " " + a2.getPoid());
        a2.emettreSon(); // C'est la méthode emmetreSon de Chien qui sera appelée

        // DownCasting: parent -> enfant
        if (a2 instanceof Chien) {  // test si a2 est de "type" Chien
            Chien ch2 = (Chien) a2; // Pour passer d'une super-classe à une sous-classe, il faut le faire explicitement avec un cast
            ch2.emettreSon();
            System.out.println(ch2.getNom()); // avec la référence ch2 (de type Chien), on a bien accès à toutes les méthodes de la classe Chien
        }

        // Java 16 -> pattern matching instanceof
        if (a2 instanceof Chien ch2) {
            System.out.println("---------");
            ch2.emettreSon();
            System.out.println(ch2.getNom());
        }

        Refuge refuge = new Refuge();
        refuge.ajouter(new Chien(5, 3000, "Laika"));
        refuge.ajouter(new Chien(5, 7000, "Rolo"));
        refuge.ajouter(new Chat(5, 4000, 9));
        refuge.ajouter(new Canard(2, 3000));
        refuge.ecouter();

        // Object
        // toString
        System.out.println(ch1.toString());
        System.out.println(ch1);
        // Equals
        Animal a3 = new Chien(5, 7000, "Rolo");
        Animal a4 = new Chien(5, 7000, "Rolo");
        Animal a5 = a3;

        // == avec des objet -> compare les références des objets
        System.out.println(a3 == a4); // false, on compare les références (a3 et a4 sont 2 objets, leurs références sont différente)
        System.out.println(a3 == a5); // true, a3 et a5 référence le même objet

        // pour comparer des objets, il faut utiliser equals
     
        // - equals de Object -> comparaison des références
        
        // - equals est redéfini dans la classe Animal et dans la classe Chien
        // true on compare le contenu des 2 objets (poid, age, nom)
        System.out.println(a3.equals(a4));

        // clone
        try {
            Chien ch5 = (Chien) ch1.clone();
            System.out.println(ch1);
            System.out.println(ch5);
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }

        // Interface
        // On peut utiliser une référence vers une interface pour référencer une classe qui l'implémente
        PeutMarcher[] pm = new PeutMarcher[5]; // tableau de 5 référence vers des objets qui implémente L'interface peut marcher
        // L'objet chien n'est vu que comme un interface PeutMarcher, on ne peut utiliser que les méthode de PeutMarcher (marcher, courrir)
        pm[0] = new Chien(5, 7000, "Rolo");
        pm[1] = new Chien(5, 3000, "Idefix");
        pm[2] = new Chat(7, 4000, 6);
        pm[3] = new Canard(4, 2500);
        for (var p : pm) {
            if (p != null) {
                p.courrir();
            }
        }

        // à partir de Java 8
        // Variable d'interface
        System.out.println(PeutMarcher.VITESSE_MAXIMUM);

        // Méthode d'interface
        System.out.println(PeutMarcher.getNombrePatte());
        // System.out.println(Chien.getNombrePatte());

        // Méthode par défaut (java 8)
        ch1.ramper();

        Chat chat1 = new Chat(5, 3000, 4);
        chat1.ramper();

        Canard cn1 = new Canard(5, 3000);
        cn1.ramper();

        // Record -> syntaxe compact pour écrire une classe immuable
        Personne p1 = new Personne("John", "Doe", LocalDate.of(1994, 10, 23));
        System.out.println(p1.nom());
        System.out.println(p1);
        System.out.println(p1.getAge());

        // Enumération
        Motorisation mt = Motorisation.GPL;

        // name ou toString : Enumeration -> String
        System.err.println(mt.name());
        System.out.println(mt); //mt.toString

        // ordinal : index de la valeur selon l'ordre de déclaration
        System.out.println(mt.ordinal()); // 0

        // valueOf : String -> Enumeration
        Motorisation mt2 = Motorisation.valueOf("DIESEL");
        System.out.println(mt2.toString());

        // values() : retourne un tableau de toutes les valeurs énumérées disponibles
        Motorisation[] mt3 = Motorisation.values();
        for (var m : mt3) {
            System.out.println(m);
        }
    }

}
