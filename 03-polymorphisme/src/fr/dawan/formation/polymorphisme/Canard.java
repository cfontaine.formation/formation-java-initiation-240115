package fr.dawan.formation.polymorphisme;
import fr.dawan.formation.polymorphisme.interfaces.PeutMarcher;

public class Canard extends Animal implements PeutMarcher {

    public Canard(int poid, int age) {
        super(poid, age);
    }

    @Override
    public void emettreSon() {
        System.out.println("Coin coin");
    }
    
    @Override
    public void marcher() {
        System.out.println("Le canard marche");
    }

    @Override
    public void courrir() {
        System.out.println("Le canard court");

    }

    @Override
    public void ramper() {
        System.out.println("????????? [Canard]");
    }
    
    

}
