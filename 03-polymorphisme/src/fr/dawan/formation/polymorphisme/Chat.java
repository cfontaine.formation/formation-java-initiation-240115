package fr.dawan.formation.polymorphisme;
import fr.dawan.formation.polymorphisme.interfaces.Deplacer;
import fr.dawan.formation.polymorphisme.interfaces.PeutMarcher;

public class Chat extends Animal implements PeutMarcher, Deplacer {

    private int nbVie = 9;

    public Chat(int poid, int age, int nbVie) {
        super(poid, age);
        this.nbVie = nbVie;
    }

    public int getNbVie() {
        return nbVie;
    }

    public void setNbVie(int nbVie) {
        this.nbVie = nbVie;
    }

    @Override
    public void emettreSon() {
        System.out.println("Le Chat miaule");
    }

    @Override
    public void marcher() {
        System.out.println("Le chat marche");
    }

    @Override
    public void courrir() {
        System.out.println("Le chat court");

    }

    @Override
    public void ramper() {
        // 2 interfaces par défaut, on a la même méthode ramper
        // pour lever l'ambiguité, il faut choisir la méthode par défaut de l'interface
        // PeutMarcher.super.ramper(); // la méthode de ramper de PeutMarcher
        // Deplacer.super.ramper(); // la méthode de ramper de Deplacer
        System.out.println("Le chat rampe"); // ou on écrit un code spécifique pour la classe
    }

}
