package fr.dawan.formation.polymorphisme;
import java.util.Objects;

//Animal est une classe abstraite, elle ne peut pas être instantiée elle même
//mais uniquement par l'intermédiaire de ses classes filles
public abstract class Animal {

    private int poid;

    private int age;

    public Animal(int poid, int age) {
        this.poid = poid;
        this.age = age;
    }

    public int getPoid() {
        return poid;
    }

    public void setPoid(int poid) {
        this.poid = poid;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    // Méthode abstraite -> doit obligatoirement être redéfinit par les classes
    // filles
    public abstract void emettreSon(); /*
                                        * { System.out.println("L'animal emet un son"); }
                                        */

    // Redéfinition de la méthode toString de Object
    @Override
    public String toString() {
        return "Animal [poid=" + poid + ", age=" + age + "]";
    }

    // Redéfinition de la méthode hashCode et de equals
    // 2 objets egal avec equals ont le même hashcode et
    // l'inverse n'est pas toujours vrai:
    // 2 objets qui ont un hashcode ne sont pas toujours égaux
    @Override
    public int hashCode() {
        return Objects.hash(age, poid);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Animal other = (Animal) obj;
        return age == other.age && poid == other.poid;
    }

}
