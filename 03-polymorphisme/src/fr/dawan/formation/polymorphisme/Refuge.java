package fr.dawan.formation.polymorphisme;

public class Refuge {

    private Animal[] places = new Animal[20];

    private int placeOccupe;

    public void ajouter(Animal a) {
        if (placeOccupe < places.length) {
            places[placeOccupe] = a;
            placeOccupe++;
        }
    }

    public void ecouter() {
        for (int i = 0; i < placeOccupe; i++) {
            places[i].emettreSon();
        }
    }
}
