package fr.dawan.formation.polymorphisme.interfaces;

public interface Deplacer {

    // Méthode par défaut (java 8)
    default void ramper() {
        System.out.println("Méhode par défaut [Déplacer]");
    }

}
