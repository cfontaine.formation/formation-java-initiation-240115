package fr.dawan.formation.polymorphisme.interfaces;

public interface PeutMarcher {

    // Variable d'interface (java 8)
    int VITESSE_MAXIMUM =36;
    
    // Méthode d'interface (java 8)
    static int getNombrePatte() {
        return 4;
    }
    
    void marcher();
    
    void courrir();
    
    default void ramper() {
        System.out.println("Méthode par défaut ramper [PeutMarcher]");
    }
}
