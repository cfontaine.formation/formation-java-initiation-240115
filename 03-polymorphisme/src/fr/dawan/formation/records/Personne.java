package fr.dawan.formation.records;
import java.time.LocalDate;
import java.time.Period;

public record Personne(String prenom, String nom, LocalDate dateNaissance) {
    
    public Personne() {
        this("Jane","Doe",LocalDate.of(1990, 3, 10));
    }
    
    public int getAge() {
       return Period.between(dateNaissance, LocalDate.now()).getYears();
    }
}
