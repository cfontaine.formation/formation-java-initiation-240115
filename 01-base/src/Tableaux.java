import java.util.Scanner;

public class Tableaux {

    public static void main(String[] args) {
        // Tableau à une dimension

        // Déclaration d'un tableau à une dimension
        // double[] t=null; // On déclare une référence vers le tableau
        // t=new double[5]; // On crée l'objet tableau de double

        double[] t = new double[5]; // ou double tab[]=new double[5];
        // valeur d'initialisation des éléments du tableau
        // entier -> 0
        // double ou float -> 0.0
        // char -> '\u0000'
        // boolean -> false
        // reference -> null

        // Accèder à un élément du tableau
        System.out.println(t[2]); //// affichage du 3ème élément du tableau
        t[1] = 1.23;

        // Indice en dehors du tableau -> exception
        // System.out.println(t[40]);

        // Nombre d'élément d'un tableau à une dimension
        System.out.println("Nb élément= " + t.length);

        // Parcourir un tableau
        for (int i = 0; i < t.length; i++) {
            System.out.println("t[" + i + "]=" + t[i]);
        }
        // Parcourir un tableau avec un "foreach" uniquement en lecture pour les types primitifs
        for (double d : t) {
            System.out.println(d);
            d = 3.1419; // la modification de d ne modifie pas le tableau
            System.out.println(d);
        }

        for (var d : t) {
            System.out.println(d);
        }

        // Déclaration et initialisation d'un tableau
        char[] tChr = { 'a', 'z', 'e', 't' };
        for (var c : tChr) {
            System.out.println(c);
        }

        // On peut utiliser une variable entière pour définir la taille du tableau lors de la déclaration
        int s = 10;
        int[] ti = new int[s]; // tableau de 10 éléments

        // 1 - Trouver la valeur maximale et la moyenne d’un tableau de 5 entiers:
        // -7,-4,-8,-2,-3
        // 2 - Modifier le programme pour faire la saisie de la taille du tableau et
        // saisir les éléments du tableau

        Scanner scan = new Scanner(System.in);
        System.out.print("Saisir la taille du tableau=");

        int size; // 2
        do {
            size = scan.nextInt();
        } while (size <= 0);
        int tab[] = new int[size];
        for (int i = 0; i < size; i++) {
            System.out.print("tab[" + i + "]=");
            tab[i] = scan.nextInt();
        }
        // int[] tab= {-7,-4,-8,-2,-3}; // 1
        int max = tab[0]; // Integer.MIN_VALUE;
        double somme = 0.0;
        for (var elm : tab) {
            if (elm > max) {
                max = elm;
            }
            // ou
            // max = Math.max(elm, max);
            somme += elm;
        }
        double moyenne = somme / tab.length;
        System.out.println("Maximum= " + max + " Moyenne= " + moyenne);

        // Tableau à 2 dimensions

        // Déclaration d'un tableau à 2 dimensions
        int[][] t2d = new int[3][4];
//      ou  int t2d[][] = new int[3][4];
//      ou  int[] t2d[] = new int[3][4];

        // Accèder à un élément
        System.out.println(t2d[1][2]);
        t2d[0][0] = 42;

        // Nombre d'élément sur la première dimension => nombre de ligne
        System.out.println(t2d.length); // 3
       
        // Nombre de colonne de la première ligne
        System.out.println(t2d[0].length); // 4

        // Parcourir un tableau en 2 dimensions
        for (int r = 0; r < t2d.length; r++) {
            for (int c = 0; c < t2d[r].length; c++) {
                System.out.print(t2d[r][c] + "\t");
            }
            System.out.println();
        }

        for (int[] row : t2d) {
            for (int e : row) {
                System.out.print(e + "\t");
            }
            System.out.println();
        }

        // Déclaration et initialisation d'un tableau à 2 dimensions
        double[][] t2d2 = { { 1.6, 5.4, 7.8 }, { 3.4, 7.1, 4.3 } };
        for (var row : t2d2) {
            for (var e : row) {
                System.out.print(e + "\t");
            }
            System.out.println();
        }

        // Tableau 3d
        int[][][] t3d = new int[4][3][2];

        // Tableau en escalier
        // Déclaration d'un tableau en escalier => chaque ligne a un nombre d'élement différent
        int[][] tEsc = new int[3][];
        tEsc[0] = new int[2];
        tEsc[1] = new int[4];
        tEsc[2] = new int[3];

        // Parcourir un tableau en escalier
        for (int r = 0; r < tEsc.length; r++) {
            for (int c = 0; c < tEsc[r].length; c++) {
                System.out.print(tEsc[r][c] + "\t");
            }
            System.out.println();
        }

        // Déclaration et initialisation d'un tableau
        int[][] tEsc2 = { { 4, 7 }, { 4, 8, 3 }, { 5, 6 } };
        for (int r = 0; r < tEsc2.length; r++) {
            for (int c = 0; c < tEsc2[r].length; c++) {
                System.out.print(tEsc2[r][c] + "\t");
            }
            System.out.println();
        }

        // var avec un tableau
        // var nombres[]= {2,5,6}; // ne compile pas
        var nombres = new int[] { 3, 7, 5 };
        System.out.println(nombres[0]);
        var tv = nombres;
        System.out.println(tv[0]);
        scan.close();
    }
}
