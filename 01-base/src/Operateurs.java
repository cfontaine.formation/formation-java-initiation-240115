import java.util.Scanner;

public class Operateurs {

    public static void main(String[] args) {
        // Opérateur arithmétique
        int a = 1;
        int b = 2;
        int res = a + b;
        System.out.println(res);
        System.out.println(res % 2); // 1 -> % reste de la division entière

        // Division par 0
        // - Entier
        // System.out.println(10/0); // -> Exception

        // - Nombre à virgule flottante
        System.out.println(1.0 / 0.0); // INFINITY
        System.out.println(-1.0 / 0.0); // -INFINITY
        System.out.println(0.0 / 0.0); // NAN

        // Incrémentation/Décrémentation

        // Pré-incrémention
        int inc = 0;
        // Incrémentation de inc et affectation de r avec la valeur de inc
        int r = ++inc; // inc=1 r=1
        System.out.println(inc + " " + r);

        // Post-incrémentation
        inc = 0;
        // Affectation de r avec la valeur de inc et incréméntation de inc
        r = inc++; // r=0 inc=1
        System.out.println(inc + " " + r);

        // Affectation composé
        int af = 12;
        af += 10; // correspond à af= af + 10
        System.out.println(af);

        // Opérateur comparaison
        // Une comparaison a pour résultat un booléen
        boolean test = af > 5; // true
        System.out.println(test);

        // Opérateur logique
        // ! -> Opérateur non
        System.out.println(!test); // false

        // c1 c2 | ET | OU | Ou exclusif
        // -------------------------------------------------
        // faux faux | faux | faux | faux
        // faux vrai | faux | vrai | vrai
        // vrai faux | faux | vrai | vrai
        // vrai vrai | vrai | vrai | faux

        // Opérateur court circuit && et ||
        // && -> ET
        System.out.println(af);
        test = af > 100 && ++af > 0;
        // && dés qu'une comparaison est fausse, les suivantes ne sont pas évaluée
        System.out.println(af);
        System.out.println(test);

        // || -> OU
        // || dés qu'une comparaison est vrai, les suivantes ne sont pas évaluées
        test = af < 1000 || af == -6;
        System.out.println(test);

        // Littéraux entier -> changement
        int dec = 12; // par défaut -> base 10
        int hexa = 0xff53ef; // 0x -> base 16 héxadécimal
        int octal = 045; // 0 -> base 8 octal
        int bin = 0b1010101; // 0b -> base 2 binaire
        System.out.println(dec + " " + hexa + " " + octal + " " + bin);

        // Opérateur binaire
        byte bn = 0b10101;
        System.out.println(bn);
        System.out.println(Integer.toBinaryString(~bn) + " " + (~bn + 1)); // complémént -> 11111111 11111111 11111111
                                                                           // 11101010
        System.out.println(Integer.toBinaryString(bn & 0b11001)); // 10001
        System.out.println(Integer.toBinaryString(bn | 0b11001)); // 11101
        System.out.println(Integer.toBinaryString(bn ^ 0b11001)); // 1100 -> Ou exclusif
        // Décallage à gauche de 2 bit, on insère un 00 à droite
        System.out.println(Integer.toBinaryString(bn << 2)); // 1010100
        // Décallage à droite de 1 bits, on insére le bit de signe à gauche ici 0
        System.out.println(Integer.toBinaryString(bn >> 1)); // 1010

        // Promotion numérique
        // 1=> Le type le + petit est promu vers le + grand type des deux
        int pri = 34;
        long prl = 456L;
        long s1 = pri + prl; // 4=> Après une promotion le résultat aura le même type
        System.out.println(s1);

        // 2=> La valeur entière est promue en virgule flottant
        double prd = 3.14;
        double s2 = pri + prd;
        System.out.println(s2);

        // 3=> byte, short, char sont promus en int
        short sh1 = 3;
        short sh2 = 4;
        int s3 = sh1 + sh2;
        System.out.println(s3);

        int pr4 = 11;
        int pres1 = pr4 / 2; // 5
        double pres2 = pr4 / 2.0; // 5.5
        double pres3 = ((double) pr4) / 2; // 5.5

        // On peut afficher les résultats en faisant des concaténations avec des litéral
        // chaine de caractères
        System.out.println("pres1=" + pres1 + " pres2=" + pres2 + "pres3=" + pres3);

        // Formater l'affichage -> printf
        // %s Permet de formater une chaine de caractères
        // %c Permet de formater un caractère
        // %d Permet de formater un entier en base décimale
        // %x Permet de formater un entier sous forme hexadecimale
        // %e Permet de formater un réel sous forme déciamele en notation scientifique
        // %f Permet de formater un réel sousforme décimale
        // %t Permet de formater les dates

        // Le caractère "\" est un caractère d'échappement permettant d'écrire des
        // caractères spéciaux dans une chaine de caractères

        System.out.printf("pres1= %d pres2= %f\n", pres1, pres2);

        // Saisie dans la console => Scanner
        Scanner sc = new Scanner(System.in);
        int v = sc.nextInt();
        double dd = sc.nextDouble();
        String str = sc.next();

        System.out.println(v + " " + dd + " " + str);

        // Déclaration de variable et Opérateur
        // Exercice: Moyenne
        // Saisir 2 nombre entier et afficher la moyenne des 2 nombres
        System.out.println("Saisir 2 nombres entiers:");
        int v1 = sc.nextInt();
        int v2 = sc.nextInt();
        double vr = (v1 + v2) / 2.0;
        System.out.println("Moyenne=" + vr);

        sc.close();
    }

}
