public class TypeBase {

    // Méthode main => point d'entré du programme
    public static void main(String[] args) {

        // Déclaration variable
        int i;

        // System.out.println(i); // En java, on ne peut pas utiliser une variable non
        // initialisée

        // Initialisation de la variable
        i = 42;
        System.out.println(i);

        // Déclaration et initialisation d'une variable
        double d = 1.23;
        System.out.println(d);

        // Déclaration multiple
        double hauteur = 12.34, largeur = 3.0;
        System.out.println("largeur=" + largeur + "hauteur=" + hauteur);

        // Littérale
        // Littérale entière est par défaut de type int
        long l = 12345678912345L; // L -> long
        System.out.println(l);

        // Littéral réel
        double d1 = 1.3;
        double d2 = .5;
        double d3 = 2.4e3; // 2400.0
        System.out.println(d1 + " " + d2 + " " + d3);

        // Littéral réel est par défaut de type double
        float f = 1.2F; // F -> littéral de type float
        System.out.println(f);

        // Littéral caractère
        char chr = 'a';
        char chrUTF8 = '\u0061';
        System.out.println(chr + " " + chrUTF8);

        // Littéral boolean
        boolean b = true; // false
        System.out.println(b);

        // Inférence de type -> var
        // Le type de la variable est déterminé par le compilateur à partir du type de
        // la valeur d'initialisation
        var iv = 123; // iv -> entier
        var iv2 = 123L; // iv2 -> long
        var iv3 = 4.5F; // iv3 -> float
        System.out.println(iv + " " + iv2 + " " + iv3);

        // Transtypage
        // Transtypage implicite -> pas de perte de donnée

        // Type inférieur vers un type supérieur
        int ti1 = 34;
        long ti2 = ti1;

        // Entier vers un réel
        double td1 = ti1;

        System.out.println(ti1 + " " + ti2 + " " + td1);

        // Transtype explicite cast => (nouveauType)

        // Type supérieur vers un type inférieur
        int te1 = 34;
        short se1 = (short) te1; // 34
        System.out.println(se1);

        // Réel vers un Entier
        double td2 = 1.23;
        int te2 = (int) td2; // 1
        System.out.println(te2);

        // Transtypage explicite: Dépassement de capacité
        int dep1 = 300; // 00000000 00000000 00000001 00101100 ->300
        byte dep2 = (byte) dep1; // 44
        System.out.println(dep1 + " " + dep2);

        // Type référence
        StringBuilder sb1 = new StringBuilder("hello");
        StringBuilder sb2 = null; // sb2 ne référence aucun objet
        System.out.println(sb1 + " " + sb2);

        sb2 = sb1; // sb1 et sb2 font références au même objet
        System.out.println(sb1 + " " + sb2);

        sb1 = null;
        System.out.println(sb1 + " " + sb2);
        sb2 = null;
        System.out.println(sb1 + " " + sb2);
        // sb1 et sb2 sont égales à null
        // Il n'y a plus de référence sur l'objet => Il éligible à la destruction par le
        // garbage collector

        // Les types enveloppes (Wrapper)
        // Integer wi=new Integer(42); // deprecated
        Integer iWarp = Integer.valueOf(42);
        System.out.println(iWarp);
        
        // Convertion String -> primitif
        double dPrim=Double.parseDouble("12.34");
        System.out.println(dPrim);
        
        // Convertion String -> wrapper
        Double dWarp= Double.valueOf("12.45");
        System.out.println(dWarp);
        
        // Conversion Double vers long
        long ll=dWarp.longValue();
        System.out.println(ll);
        
        String strBin=Integer.toBinaryString(12);
        String strOct=Integer.toOctalString(12);
        String strHex=Integer.toHexString(12);
        System.out.println(strBin + " " + strOct + " " +strHex);
        
        // autoboxing: Conversion automatique primitif -> Wraper
        Integer autoBox=23; // autoboxing
        System.out.println(autoBox);
        
        // unboxing: Conversion automatique Wraper -> primitif
        int unBox=autoBox; // unboxing
        System.out.println(unBox);
        
        // Tache
        // vue: menu -> windows -> préférences -> java -> compiler -> task tag
        // TODO une fontionalité à ajouter
        // FIXME une correction à faire
    }
}
