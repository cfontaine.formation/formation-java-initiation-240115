import java.util.Scanner;

public class Instructions {

    public static void main(String[] args) {
        // Saisie dans la console => Scanner
        Scanner sc = new Scanner(System.in);

        // Condition: if
        System.out.print("[Condition if]\nSaisir une valeur ");
        double d = sc.nextDouble();

        if (d > 10.0) {
            System.out.println("d est supérieur à 10");
        } else if (d == 10.0) {
            System.out.println("d est égal à 10");
        } else {
            System.out.println(" d est inférieur à 10");
        }

        // Exercice: Nombre Pair
        // Créer un programme qui indique, si le nombre entier saisie dans la console est paire ou impaire
        System.out.print("\n[Nombre pair]\nSaisir un nombre entier ");
        int v = sc.nextInt();
        if (v % 2 == 0) { // (v & 0x1)==0
            System.out.println("Nombre pair");
        } else {
            System.out.println("Nombre impair");
        }

        // Exercice: Intervalle
        // Saisir un nombre et indiquer s'il fait partie de l'intervalle -4 (exclus) et 7 (inclus)
        System.out.print("\n[Intervalle]\nSaisir un nombre entier ");
        int vi = sc.nextInt();
        if (vi > -4 && vi <= 7) {
            System.out.println(vi + " fait partie de l'intervalle");
        }

        // Condition: opérateur ternaire
        System.out.print("\n[Valeur absolue]\nSaisir un nombre décimal ");
        double val = sc.nextDouble();
        String abs = "|val|=" + (val >= 0.0 ? val : -val);
        System.out.println(abs);

        // Condition: Switch
        System.out.print("\n[Switch]\n\"Numéro du jour= ");
        int jours = sc.nextInt();
        switch (jours) {
        case 1:
            System.out.println("Lundi");
            break; // s'il n'y a pas de break le code continura à s'executer jusqu'a
                   // la fin du switch ou jusqu'au prochain break
        case 6:
        case 7:
            System.out.println("week end !");
            break;
        default:
            System.out.println("autre jour");
        }

        // à partir de java 14
        // case valeur ->, pas de break
        switch (jours) {
        case 1 -> System.out.println("Lundi");
        case 6, 7 -> {
            System.out.println("week end !");
            System.out.println("à lundi");
        }
        default -> System.out.println("autre jour");
        }

        // Switch expression, à partir de java 14
        String nomJour = switch (jours) {
        case 1 -> "Lundi";
        case 6, 7 -> {
            System.out.println(jours);
            yield "week end !"; // Dans un bloc de code, yield pour préciser la valeur à retourner
        }
        default -> "autre jour";
        };
        System.out.println(nomJour);

        // Avec la syntaxe classique, on utilise le mot clef yield pour préciser la valeur à retourner
        nomJour = switch (jours) {
        case 1:
            yield "Lundi";
        case 6, 7:
            yield "week end !";
        default:
            yield "autre jour";
        };

        System.out.println(nomJour);

        // Exercice Calculatrice
        // Faire un programme calculatrice
        // Saisir dans la console
        // - un double v1
        // - une chaine de caractère opérateur qui a pour valeur valide : + - * /
        // - un double v2
        // Afficher:
        // - Le résultat de l’opération
        // - Une message d’erreur si l’opérateur est incorrecte
        // - Une message d’erreur si l’on fait une division par 0
        System.out.println("\n[Calculatrice]");
        double a = sc.nextDouble();
        String op = sc.next();
        double b = sc.nextDouble();

        switch (op) {
            case "+" -> System.out.println(a + " + " + b + " = " + (a + b));
            case "-" -> System.out.println(a + " - " + b + " = " + (a - b));
            case "*" -> System.out.println(a + " x " + b + " = " + (a * b));
            case "/" -> {
                // pour tester l'égalité ou la différence de nombre réel provenant d'un calcul
                // (b>-0.00000001 && b<0.00000001)
                if (b == 0.0) {
                    System.out.println("Division par 0");
                } else {
                    System.out.println(a + " / " + b + " = " + (a / b));
                }
            }
            default -> System.out.println("l'opérateur " + op + " n'est pas correct");
        }

        String res = switch (op) {
            case "+" -> a + " + " + b + " " + (a + b);
            case "-" -> a + " - " + b + " " + (a - b);
            case "*" -> a + " x " + b + " " + (a * b);
            case "/" -> b == 0.0 ? "Division par 0" : a + " / " + b + " " + (a / b);
            default -> "l'opérateur " + op + " n'est pas correct";
        };
        System.out.println(res);

        // Boucle: while / do while

        int j = 0;
        while (j < 10) {
            System.out.println(j);
            j++;
        }

        j = 0;
        do {
            System.out.println(j);
            j++;
        } while (j < 10);

        // Boucle: for
        for (int i = 10; i > 0; i--) {
            System.out.println(i);
        }

        // Instruction de saut
        // break
        for (int i = 0; i < 10; i++) {
            if (i == 2) {
                break; // break => termine la boucle
            }
            System.out.println(i);
        }

        // continue
        for (int i = 0; i < 10; i++) {
            if (i == 2) {
                continue; // continue => on passe à l'itération suivante
            }
            System.out.println(i);
        }

        // Label
        EXIT_LOOP: for (int i = 0; i < 10; i++) {
            for (int k = 0; k < 5; k++) {
                if (i == 2) {
                    break EXIT_LOOP; // se branche sur le label EXIT_LOOP et quitte les 2 boucles imbriquées
                }
                System.out.println(i + " " + k);
            }
        }

        // Exercice: Table de multiplication
        // Faire un programme qui affiche la table de multiplication pour un nombre entre 1 et 9
        //
        // 1 X 4 = 4
        // 2 X 4 = 8
        // …
        // 9 x 4 = 36
        //
        // Si le nombre saisie est en dehors de l’intervalle 1 à 9 on arrête
        //  sinon on redemande une nouvelle valeur
        System.out.println("\n[Table de multiplication]");
        for (;;) { // while(true)
            int mul = sc.nextInt();
            if (mul < 1 || mul > 9) {
                break;
            }
            for (int i = 1; i < 10; i++) {
                System.out.println(i + " x " + mul + " = " + (i * mul));
            }
        }

        // Exercice: Quadrillage
        // un quadrillage dynamiquement on saisit le nombre de colonne et le nombre de ligne

        // ex: pour 2 3
        // [ ][ ]
        // [ ][ ]
        // [ ][ ]
        System.out.print("\n[Quadrillage]\nNombre de colonne= ");
        int col = sc.nextInt();
        System.out.print("Nombre de ligne= ");
        int row = sc.nextInt();
        for (int l = 0; l < row; l++) {
            for (int c = 0; c < col; c++) {
                System.out.print("[ ]");
            }
            System.out.println();
        }
        sc.close();
    }

}
