import java.util.Scanner;

public class Methodes {

    public static void main(String[] args) {
        // Appel d'une méthode
        double res = multiplier(2.0, 3.0);
        System.out.println(res);

        // Appel d'une méthode sans type retour (void)
        afficher(res);

        // Test du passage des arguments par valeurs
        int val = 23;
        testParamValeurPrim(val); // La valeur contenu dans val est copier dans le paramètre (v)
        System.out.println(val); // le contenu de val ne peut pas être modifié par la méthode => passage par
                                 // valeur

        // Test des arguments par valeurs (paramètre de type référence)
        StringBuilder s = new StringBuilder("hello");
        testParamValeurRef(s);
        System.out.println(s);

        // Test des arguments par valeurs (Modification de l'état de l'objet)
        testParamValueRefObj(s);
        System.out.println(s);

        Scanner sc = new Scanner(System.in);
        // Exercice: appel de la méthode maximum
//        System.out.println("\n[Maximum]\nSaisir 2 entiers");
//        int v1 = sc.nextInt();
//        int v2 = sc.nextInt();
//        System.out.println("Maximum=" + maximum(v1, v2));

        // Exercice: appel de la méthode paire
        System.out.println(even(9));
        System.out.println(even(4));

        // Nombre d'arguments variables
        System.out.println(moyenne(5.0, 10.0, 8.0));
        System.out.println(moyenne(15.0, 10.0));
        System.out.println(moyenne(12.0));
        System.out.println(moyenne());
        double[] t = { 13.0, 6.5, 9.5 };
        System.out.println(moyenne(t));

        // Surcharge de méthodes

        // Correspondance exacte
        System.out.println(somme(1, 2));
        System.out.println(somme(1.5, 2.6));
        System.out.println(somme(3.5, 6));
        System.out.println(somme("aze", "rty"));
        System.out.println(somme(1, 2, 5));

        // Si le compilateur ne trouve pas de correspondance exacte avec le type des
        // paramètres
        // Il effectue des conversions automatiques pour trouver la méthode à appeler
        System.out.println(somme(2L, 5));
        System.out.println(somme(23L, 52L));
        System.out.println(somme('a', 'b'));

        // Si le compilateur ne trouve pas de méthode correspondante aux paramètres =>
        // ne compile pas
        // System.out.println(somme("hello",2));

        // Affichage des paramètres passés à la méthode main
        for (var str : args) {
            System.out.println(str);
        }

        // Méthode Récursive
        int fac = factorial(3);
        System.out.println(fac);

        menu(sc);

        sc.close();
    }

    // Déclaration d'une méthode
    static double multiplier(double d1, double d2) {
        return d1 * d2; // return => Interrompt l'exécution de la méthode
                        // => Retourne la valeur (expression à droite)
    }

    static void afficher(double d) {
        System.out.println(d);
        // return; // avec void => return; ou return peut être omis
    }

    // Test des arguments par valeurs (paramètre de type primitif)
    static void testParamValeurPrim(int v) {
        System.out.println(v);
        v = 100; // la modification du paramètre val n'a pas de répercution en dehors de la
                 // méthode
        System.out.println(v);
    }

    // Test des arguments par valeurs (paramètre de type référence
    static void testParamValeurRef(StringBuilder sb) {
        System.out.println(sb);
        sb = new StringBuilder("modif");
        // si on modifie la valeur de la référence sb, il n'a pas répercution en dehors
        // de la méthode la référence est modifiée
        System.out.println(sb);
    }

    // Test des arguments par valeurs (Modification de l'état de l'objet)
    public static void testParamValueRefObj(StringBuilder sb) {
        System.out.println(sb);
        // si on modifie l'état de l'objet, il a une répercution en dehors de la méthode
        // (la référence n'est pas modifiée)
        sb.append("et Other Value");
        System.out.println(sb);
    }

    // Exercice Maximum
    // Écrire une fonction maximum qui prends en paramètre 2 nombres et retourne le
    // maximum
    static int maximum(int a, int b) {
        return a > b ? a : b;
        // ou
//        if (a > b) {
//            return a;
//        } else {
//            return b;
//        }
    }

    static boolean even(int val) {
        return val % 2 == 0;
        // ou
//        if(val%2==0) {
//            return true;
//        }
//        else {
//            return false;
//        }
    }

    // Nombre d'arguments variable
    static double moyenne(double... notes) {
        // dans la méthode notes est considérée comme un tableau
        double somme = 0.0;
        for (var n : notes) {
            somme += n;
        }
        return somme / notes.length;
    }

    // Surcharge de méthode
    // Une méthode peut être surchargée => plusieurs méthodes peuvent avoir le même
    // nom, mais leurs signatures doient être différentes
    // La signature d'une méthode correspond aux types et nombre de paramètres
    // Le type de retour ne fait pas partie de la signature
    static int somme(int a, int b) {
        System.out.println("Somme 2 entiers");
        return a + b;
    }

    static double somme(double a, double b) {
        System.out.println("Somme 2 double");
        return a + b;
    }

    static double somme(double d1, int i1) {
        System.out.println("Somme un double et un entier");
        return d1 + i1;
    }

    static String somme(String str1, String str2) {
        System.out.println("Somme 2 chaine de caractères");
        return str1 + str2;
    }

    static int somme(int i1, int i2, int i3) {
        System.out.println("Somme 3 entiers");
        return i1 + i2 + i3;
    }

    // Méthode récursive
    static int factorial(int n) { // factoriel= 1* 2* … n
        if (n <= 1) { // condition de sortie
            return 1;
        } else {
            return factorial(n - 1) * n;
        }
    }

    static void afficherTab(int[] tab) {
        System.out.print("[ ");
        for (var e : tab) {
            System.out.print(e + " ");
        }
        System.out.println("]");
    }

    static int[] saisirTab(Scanner scan) {
        int size;
        do {
            System.out.print("Saisir le nombre d'élément du tableau ");
            size = scan.nextInt();
        } while (size <= 0);

        int tab[] = new int[size];

        for (int i = 0; i < tab.length; i++) {
            System.out.print("tab[" + i + "] = ");
            tab[i] = scan.nextInt();
        }
        return tab;
    }

    static int maximumTab(int[] tab) {
        int max = Integer.MIN_VALUE;
        for (var e : tab) {
            if (e > max) {
                max = e;
            }
        }
        return max;
    }

    static double moyenneTab(int[] tab) {
        double somme = 0.0;
        for (var e : tab) {
            somme += e;
        }
        return somme / tab.length;
    }

    static void afficherMenu() {
        System.out.println("1. Saisie du tableau");
        System.out.println("2. Afficher le tableau");
        System.out.println("3. Calculer le maximum et la moyenne");
        System.out.println("4. Quitter");
    }

    static void menu(Scanner scan) {
        int[] t = null;
        int choix = 0;
        afficherMenu();
        do {
            choix = scan.nextInt();
            switch (choix) {
                case 1 -> t = saisirTab(scan);
                case 2 -> {
                    if (t != null) {
                        afficherTab(t);
                    } else {
                        System.err.println("Il n'y a pas de tableau à afficher");
                    }
                }
                case 3 -> {
                    if (t != null) {
                        System.out.println("Maximum=" + maximumTab(t) + " Moyenne=" + moyenneTab(t));
                    } else {
                        System.err.println("Il n'y a pas de tableau pour effectuer les calculs");
                    }
    
                }
                case 4 -> System.out.println("au revoir !!!");
                default -> {
                    System.err.println("Le choix " + choix + " n'existe pas");
                    afficherMenu();
                }
            }
        } while (choix != 4);
    }
}