package fr.dawan.formation;

import java.time.LocalDate;
import java.time.Month;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.StringTokenizer;

public class Application {

    public static void main(String[] args) {
        // => Chaine de caractères
        // Les chaines de caractères sont immuables une fois créée,
        // elles ne peuvent plus être modifiées
        
        String str = "Hello World"; // placer dans le string pool
        String str1 = "Hello World"; // dans le heap
        
        System.out.println(str + " " +str1);
        
        // Litéralle Chaine de caractères-> " "
        // on peut éxécuter une méthode sur une littérale
        System.out.println("hello".toUpperCase());

        // length -> Nombre de caractère de la chaine de caractère
        System.out.println(str.length());

        // Concaténation
        String strRes = str + "!!!"; // + -> concaténation
        System.out.println(strRes);

        System.out.println(str.concat("!!!!"));

        // join -> concatène les chaines, en les séparants par une chaine de
        // par une chaine de séparation
        String str3 = String.join(";", "azerty", "uiop", "wxcvb");
        System.out.println(str3);

        // split -> Découpe la chaine et retourne un tableau de sous-chaine suivant
        // un séparateur ou une expression régulière
        String[] tabStr = str3.split(";");
        for (var s : tabStr) {
            System.out.println(s);
        }

        // substring -> permet d'extraire une sous-chaine :
        // - de l'indice passé en paramètre jusqu'à la fin de la chaine
        System.out.println(str.substring(6)); // World
        // - de l'indice passé en paramètre jusqu'à l'indice de fin (exclut)
        System.out.println(str.substring(6, 8)); // Wo

        // Recherche
        // startsWith -> retourne true, si la chaine commence par la chaine passée
        // en paramètre
        System.out.println(str.startsWith("Hell")); // true
        System.out.println(str.startsWith("aaa")); // false
        System.out.println(str.endsWith("rld")); // true

        // contains -> Retourne true, si la sous-chaine passée en paramètre
        // est contenu dans la chaine
        System.out.println(str.contains("World")); // true
        System.out.println(str.contains("Azerty")); // false

        // charAt -> retourne le caractère qui corrspond à l'indice passé en paramètre
        System.out.println(str.charAt(3)); // l

        // indexOf -> retourne la première occurence de la chaine ou (de caractère)
        // passée en paramètre
        // - à partir du début de la chaine
        System.out.println(str.indexOf('o')); // 4
        // - à partir de l'indice passé en paramètre
        System.out.println(str.indexOf('o', 5)); // 7
        System.out.println(str.indexOf('o', 8)); // La sous-chaine "o" n'est pas trouvé -> -1

        // replace -> remplace toutes les sous-chaines target par replacement
        System.out.println(str.replace("Wo", "__"));

        // Mise en forme
        // toUpperCase -> retourne la chaine en majuscule
        System.out.println(str.toUpperCase());

        // trim -> supprime les caractères de blanc (espace, tabulation,
        // retour à la ligne ... ) du début et de la fin de la chaine
        String str5 = "\n  \t  \t\n Hello world \n\t\t  \n";
        System.out.println(str5);
        System.out.println(str5.trim());

        // format -> Retourne une chaine formater
        // %d -> entier, %f -> réel, %s -> string
        System.out.println(String.format("[%d -> %s]", 42, "azerty"));

        String str6 = "Bonjour";
        // Egalité de 2 chaines
        // Equals est redéfinie dans la classe String
        if (str6.equals("Bonjour")) {
            System.out.println("==");
        }

        str6 = null;
        // pour éviter un NullPointerException (appel d'une méthode sur une référence null)
        if ("Bonjour".equals(str6)) {
            System.out.println("==");
        }

        str6 = "Bonjour";
        // Comparer les chaines
        // La classe String implémente l'interface Comparable
        System.out.println(str.compareTo(str6)); // >0 H>B => retourne une valeur 6, elle correspond à la distance en
        System.out.println(str6.compareTo(str)); // <0 B<H => retourne une valeur -6
        System.out.println(str6.compareTo("Bonjour")); // ==0 B==B -> égal

        // On peut chainer les méthodes
        String strRes2 = str.toLowerCase().substring(5).replace('o', '0');
        System.out.println(strRes2);

        // StringBuilder
        // Lorsqu'il y a beaucoup de manipulation (+de 3) sur une chaine
        // ( concaténation et insertion, remplacement,supression de sous-chaine)
        // il vaut mieux utiliser un objet StringBuilder qui est plus performant qu'un
        // objet String => pas de création d'objet intermédiare
        StringBuilder sb = new StringBuilder("Hello");
        sb.append(" World");
        System.out.println(sb);
        sb.append(42);
        System.out.println(sb);
        sb.delete(4, 12);
        System.out.println(sb);
        sb.insert(4, "---------------");
        System.out.println(sb.toString());

        // StringTokenizer
        // Permet de décomposer une chaîne de caractères en une suite de mots séparés
        // par des délimiteurs

        StringTokenizer st = new StringTokenizer("aze-rty-yui-opq-sdf-ggh-jjjk", "-");

        // countTokens => renvoie le nombre d’éléments
        System.out.println(st.countTokens());
        while (st.hasMoreTokens()) {
            System.out.println(st.nextToken());
        }

        // Text BLocks (java 15)
        String avtb = " <vhdb" // avant java 15
                + "kj";
        System.out.println(avtb);

        // Text BLocks ->"""
        String tb = """
                un texte
                sur plusieurs
                lignes""";
        System.out.println(tb);

        // Exercices Chaine de caractère
        System.out.println(inverse("Bonjour"));
        System.out.println(palindrome("Bonjour"));
        System.out.println(palindrome("Radar "));
        System.out.println(acronyme("Comité international olympique", 2));
        System.out.println(acronyme("Organisation du traité de l'Atlantique Nord", 2));
        System.out.println(acronyme("Organisation des Nations unies", 3));
        System.out.println(acronyme("Developement d'application web en aglomération nantaise", 2));

        // avant java 8 Date et Calendar
        Date d = new Date();
        System.out.println(d);

        // à partir de Java 8 => LocalDate, LocalTime, LocalDateTime -> immuable

        // ces classes ne possèdent pas de constructeur, pour les instanciers
        // on utilise les méthodes de classe now ou of
        // - now => obtenir la date courante
        LocalDate dateDuJour = LocalDate.now();
        System.out.println(dateDuJour);

        // - of => créer une date spécifique
        // LocalDate finAnnee=LocalDate.of(2024, 12, 31);
        LocalDate finAnnee = LocalDate.of(2024, Month.DECEMBER, 31);
        System.out.println(finAnnee);

        // On manipule une date avec les méthodes plusXXXXX et minusXXXXX
        LocalDate d1 = dateDuJour.plusYears(1).minusMonths(1).plusWeeks(2);
        System.out.println(d1);

        // Period => représente une durée
        Period deuxAnsTroisMois = Period.of(2, 3, 0);
        System.out.println(dateDuJour.plus(deuxAnsTroisMois));

        // Différence entre 2 dates:
        // - Period
        System.out.println(Period.between(dateDuJour, finAnnee));
        // - Nombre de jours
        System.out.println(ChronoUnit.DAYS.between(dateDuJour, finAnnee));

        // format: LocalDate -> String
        System.out.println(dateDuJour.format(DateTimeFormatter.ISO_LOCAL_DATE));
        System.out.println(dateDuJour.format(DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT)));
        System.out.println(dateDuJour.format(DateTimeFormatter.ofLocalizedDate(FormatStyle.FULL)));
        System.out.println(dateDuJour.format(DateTimeFormatter.ofPattern("D dd-MM-YY")));

        // parse: String -> LocalDate
        LocalDate d3 = LocalDate.parse("2024-01-15");
        System.out.println(d3);
        d3 = LocalDate.parse("15/01/2024", DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT));
        System.out.println(d3);
    }

    public static String inverse(String str) {
        StringBuilder sb = new StringBuilder();
        for (int i = str.length() - 1; i >= 0; i--) {
            sb.append(str.charAt(i));
        }
        return sb.toString();
    }

    public static boolean palindrome(String str) {
        String tmp = str.toLowerCase().trim();
        return tmp.equals(inverse(tmp));
    }

    public static String acronyme(String str, int sizeIgnore) {
        StringBuilder sb = new StringBuilder();
        StringTokenizer st = new StringTokenizer(str.toUpperCase(), " '");
        while (st.hasMoreTokens()) {
            String tmp = st.nextToken();
            if (tmp.length() > sizeIgnore) {
                sb.append(tmp.charAt(0));
            }
        }
        return sb.toString();
    }
}
