
// final sur une classe -> on ne peut plus hérité de Voiture
public /* final */ class Voiture {

    // Variable d'instance (attribut)
    private String marque;
    private String couleur = "Rouge";
    private String plaqueIma;
    protected int vitesse;
    private int compteurKm = 20;

    // Agrégation
    private Personne proprietaire;

    // Variable de classe
    private static int cptVoiture;

    // final pour une variable d'instance ou de classe
    public static final int VITESSE_MAX = 200;

    // private final String cst="azerty";
    // ou
    private final String cst;

    // Constructeurs
    // Constructeur par défaut (sans paramètres)
    public Voiture() {
        // Si une variable d'instance final n'est pas initialisée à la déclaration
        // On peut l'initialisé dans un constructeur
        cst = "wxcvbn";
        System.out.println("Voiture constructeur par défaut");
        cptVoiture++;
    }

    // On peut surcharger le constructeur
    public Voiture(String marque, String couleur, String plaqueIma) {
        this(); // Chainage des constructeurs
        System.out.println("Voiture constructeur par 3 paramètres");
        this.marque = marque; // utilisation de this pour indiquer que l'on acccède à la variable d'instance
        this.couleur = couleur;
        this.plaqueIma = plaqueIma;
    }

    public Voiture(String marque, String couleur, String plaqueIma, int vitesse, int compteurKm) {
        this(marque, couleur, plaqueIma); // Chainage des constructeurs
        System.out.println("Voiture constructeur par 5 paramètres");
        this.vitesse = vitesse;
        this.compteurKm = compteurKm;
    }

    public Voiture(String marque, String couleur, String plaqueIma, Personne proprietaire) {
        this(marque, couleur, plaqueIma);
        this.proprietaire = proprietaire;
    }

    // Getter/Setter
    public String getMarque() {
        return marque;
    }

    public void setMarque(String marque) {
        this.marque = marque;
    }

    public String getCouleur() {
        return couleur;
    }

    public void setCouleur(String couleur) {
        this.couleur = couleur;
    }

    public String getPlaqueIma() {
        return plaqueIma;
    }

    public void setPlaqueIma(String plaqueIma) {
        this.plaqueIma = plaqueIma;
    }

    public Personne getProprietaire() {
        return proprietaire;
    }

    public void setProprietaire(Personne proprietaire) {
        this.proprietaire = proprietaire;
    }

    public int getVitesse() {
        return vitesse;
    }

    public int getCompteurKm() {
        return compteurKm;
    }

    public static int getCptVoiture() {
        return cptVoiture;
    }

    // Méthode d'instance
    public void accelerer(double vAcc) {
        if (vAcc > 0) {
            vitesse += vAcc;
        }
    }

    public void freiner(double vFrn) {
        if (vFrn > 0) {
            vitesse -= vFrn;
        }
    }

    public void arreter() {
        vitesse = 0;
    }

    public boolean estArreter() {
        return vitesse == 0;
    }

    // final sur une méthode interdit la redéfnition de cette méthode dans les classes enfants
    public /** final **/ void afficher() {
        System.out.println(
                "Voiture [" + marque + " " + couleur + " " + plaqueIma + " " + vitesse + " " + compteurKm + "]");
        if (proprietaire != null) {
            System.out.print("Propriétaire :");
            proprietaire.afficher();
        }
    }

    // Méthode de classe
    public static void testMethodeClasse() {
        System.out.println("Méthode de classe");
        // Les méthodes et les variables d'instances ne peuvent pas être utilisées
        // directement dans une méthode de classe
        // System.out.println(vitesse);
        // freiner(20);

        // Mais on peut accéder aux variables et aux autres méthodes de classe
        System.out.println(cptVoiture);
    }

    // Dans un méthode de classe on peut accèder à une variable d'instance
    // si la référence d'un objet est passée en paramètre
    public static boolean egaliteVitesse(Voiture va, Voiture vb) {
        return va.vitesse == vb.vitesse;
    }

//    private void maintenance() {
//
//    }
}