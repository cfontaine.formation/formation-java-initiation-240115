
public class Application {

    public static void main(String[] args) {
        // Appel d'une méthode de classe
        Voiture.testMethodeClasse();
        System.out.println("compteur Voiture=" + Voiture.getCptVoiture()); // Voiture.cptVoiture

        // Instantiation de la classe Voiture
        Voiture v1 = new Voiture();
        v1.afficher();
        // v1.vitesse=10; // n'est plus accessible => privé
        // Encapsulation => Pour accéder aux attributs, on doit passer par les méthodes
        // get
        System.out.println(v1.getVitesse()); // v1.vitesse
        System.out.println("compteur Voiture=" + Voiture.getCptVoiture());

        // Appel d'une méthode d'instance
        v1.accelerer(20);
        v1.afficher();
        v1.freiner(5);
        v1.afficher();
        System.out.println(v1.estArreter());
        v1.arreter();
        System.out.println(v1.estArreter());
        v1.afficher();

        Voiture v2 = new Voiture();
        System.out.println("compteur Voiture=" + Voiture.getCptVoiture());
        // v2.vitesse = 30;
        System.out.println(v2.getVitesse());

        Voiture v3 = new Voiture("Honda", "Blanc", "1045-FR-62");
        System.out.println("compteur Voiture=" + Voiture.getCptVoiture());
        v3.afficher();

        Voiture v4 = new Voiture("fiat", "jaune", "2526-az-454", 20, 500);
        System.out.println("compteur Voiture=" + Voiture.getCptVoiture());
        v4.afficher();
        v4 = null;
        // comme il n'est plus accéssible l'objet est candidat à la destruction par le
        // garbage collector
        // System.gc(); // Appel explicite au Garbage collector => à eviter

        // Méthode de classe
        System.out.println(Voiture.egaliteVitesse(v1, v3));

        // Agrégation
        Personne per1 = new Personne("John", "Doe");
        Voiture v5 = new Voiture("Ford", "Bleu", "1313-AZE-53", per1);
        v5.afficher();
        v1.setProprietaire(per1);
        v1.afficher();

        // Héritage / redéfinition de méthode
        VoiturePrioritaire vp1 = new VoiturePrioritaire("subaru", "rouge", "1235-ae-db", true);
        vp1.afficher();
        
        vp1.allumer();
        System.out.println(vp1.isGyro());
        
        System.out.println(Voiture.VITESSE_MAX);

        // Exercice: CompteBancaire
        CompteBancaire cb = new CompteBancaire(450.0, per1);
        cb.afficher();
        cb.crediter(35.0);
        cb.debiter(200.0);
        cb.afficher();
        System.out.println(cb.estPositif());

        CompteBancaire cb2 = new CompteBancaire(300.0, new Personne("Jane", "Doe"));
        cb2.afficher();
        
        CompteEpargne ce=new CompteEpargne(200.0, per1, 1.5);
        ce.afficher();
        ce.calculInterets();
        ce.afficher();
        
        // Exercice dé
        De d1=new De(6);
        De d2=new De(6,3);
        d1.afficher();
        d2.afficher();
        d2.setBloque(true);
        d1.lancer();
        d2.lancer();
        d1.afficher();
        d2.afficher();
        System.out.println(De.compareValeur(d1, d2));
        
    }

}
