
public class VoiturePrioritaire extends Voiture {

    private boolean gyro;

    public VoiturePrioritaire() {
        // super(); // Appel implicite du constructeur par défaut de la classe mère,
        // s'il existe
        System.out.println("Constructeur par défaut Voiture Prioritaire");

    }

    public VoiturePrioritaire(String marque, String couleur, String plaqueIma, boolean gyro) {
        super(marque, couleur, plaqueIma); // Appel explicite du constructeur 3 paramètres de la classe mère
        System.out.println("Constructeur  Voiture Prioritaire 4 paramètres");
        this.gyro = gyro;
    }

    public void allumer() {
        gyro = true;
    }

    public void eteindre() {
        gyro = false;
    }

    public boolean isGyro() {
        return gyro;
    }

    // Redéfinition de la méthode afficher
    @Override // -> vérification par le compilateur que c'est bien une redéfinition
    public void afficher() { // Une méthode redéfinie doit avoir la même signature que la méthode dans la
                             // classe mère
        super.afficher(); // appel de la mèthode afficher de la classe mère
        System.out.println(gyro ? "gyro allumé" : "gyro éteint");
    }

    @Override
    public void accelerer(double vAcc) {
        vitesse += 2 * vAcc;
    }

    // On ne peut pas redéfinir une méthode privé, elle est uniquement visible dans la classe où elle est déclarée
    // Si dans la classe mère et dans la classe enfant on a 2 méthodes privée qui on la même signature
    // Il n' y aucun lien entre elle
//    private void maintenance() {
//        
//    }
}
