
public class CompteEpargne extends CompteBancaire {

    private double taux;

    public CompteEpargne(double solde, Personne titulaire, double taux) {
        super(solde, titulaire);
        this.taux = taux;
    }

    public void calculInterets() {
        solde = solde * (1 + taux / 100);
    }

    @Override
    public void afficher() {
        super.afficher();
        System.out.println("Taux : " + taux);
    }

}
