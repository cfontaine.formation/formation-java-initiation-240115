
public class De {
    private int nombreFace;
    private int valeur = 1;
    private boolean bloque;

    public De(int nombreFace) {
        if (nombreFace > 1) {
            this.nombreFace = nombreFace;
        }
//        else {
//            lancer une exception
//        }
    }

    public De(int nombreFace, int valeur) {
        this(nombreFace);
        setValeur(valeur);
    }

    public int getValeur() {
        return valeur;
    }

    public void setValeur(int valeur) {
        if (valeur > 0 && valeur < nombreFace) {
            this.valeur = valeur;
        }
//      else {
//      lancer une exception
//  }
    }

    public boolean isBloque() {
        return bloque;
    }

    public void setBloque(boolean bloque) {
        this.bloque = bloque;
    }

    public int getNombreFace() {
        return nombreFace;
    }

    public void lancer() {
        if (!bloque) {
            int valAleatoire = (int) (Math.random() * nombreFace);
            valeur = valAleatoire < nombreFace ? valAleatoire + 1 : valAleatoire;
        }
    }

    public void afficher() {
        System.out.println(valeur);
    }

    public static boolean compareValeur(De de1, De de2) {
        return de1.valeur == de2.valeur;
    }

}
