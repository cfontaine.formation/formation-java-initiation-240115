
public class CompteBancaire {

    protected double solde = 50.0;
    private String iban;
    private Personne titulaire;

    private static int compteur;

    public CompteBancaire(Personne titulaire) {
        this.titulaire = titulaire;
        compteur++;
        iban = "fr59-0000-" + compteur;
    }

    public CompteBancaire(double solde, Personne titulaire) {
        this(titulaire);
        this.solde = solde;
    }

    public Personne getTitulaire() {
        return titulaire;
    }

    public void setTitulaire(Personne titulaire) {
        this.titulaire = titulaire;
    }

    public double getSolde() {
        return solde;
    }

    public String getIban() {
        return iban;
    }

    public void crediter(double valeur) {
        if (valeur > 0.0) {
            solde += valeur;
        }
    }

    public void debiter(double valeur) {
        if (valeur > 0.0) {
            solde -= valeur;
        }
    }

    public boolean estPositif() {
        return solde >= 0;
    }

    public void afficher() {
        System.out.println("Solde:" + solde);
        System.out.println("Iban:" + iban);
        if (titulaire != null) {
            System.out.print("Titulaire:");
            titulaire.afficher();
        }
    }
}
