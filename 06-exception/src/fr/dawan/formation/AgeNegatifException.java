package fr.dawan.formation;
// Créer ses propres exceptions => hériter de la classe Exception (ou une de ses sous-classses) ou de RuntimeException
public class AgeNegatifException extends Exception {

    private static final long serialVersionUID = 1L;

    public AgeNegatifException() {
        super();
    }

    public AgeNegatifException(int age) {
        super("L'age " + age + " est négatif");
    }

    public AgeNegatifException(String message) {
        super(message);
    }

    public AgeNegatifException(String message, Throwable cause) {
        super(message, cause);
    }

}
