package fr.dawan.formation;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

public class Application {

    public static void main(String[] args) {
        System.out.println("Début du programme");
        FileInputStream fi = null;

        // Checked exception -> // checked exception => on est obligé de traiter
        // l'exception
        try {
            fi = new FileInputStream("nexistepas"); // ouverture d'un fichier qui n'existe pas => lancer une exception
                                                    // FileNotFoundException
            int a = fi.read();
            // pour un bloc try, il peut y avoir plusieurs blocs catch pour traiter
            // diférentes exceptions de la - à la + générale
            System.out.println("Suite du code");
        } catch (FileNotFoundException e) { // si une exception FileNotFoundException ce produit dans le bloc try
            System.err.println("Le fichier n'existe pas");
            System.err.println(e.getMessage());
            e.printStackTrace();
        } catch (IOException e) {
            System.err.println("Impossible de lire le fichier");
        } catch (Exception e) {
            System.out.println("Une autre exception");
        } finally { // le bloc finally est toujours exécuté
            {
                System.out.println("toujours executer");
                if (fi != null) {
                    try {
                        fi.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

        }

        // Runtime Exception => on peut traiter l'exception ou pas (pas d'obligation)
        try {
            int[] t = new int[5];
            t[23] = 5;
        } catch (ArrayIndexOutOfBoundsException e) {
            System.err.println("L'indice est en dehors du tableau" + e.getMessage());
        }

        Scanner sc = new Scanner(System.in);
        int age = sc.nextInt();
        try {
            traitementEmploye(age);
            System.out.println("Suite traitement");
        } catch (EmployeException e) {
            System.err.println("Main " + e.getMessage());
            e.printStackTrace();
        }
        System.out.println("Fin du programme");
        sc.close();
    }

    public static void traitementEmploye(int age) throws EmployeException {
        System.out.println("Début Traitement Employe");
        try {
            traitementAge(age);
        } catch (AgeNegatifException e) {
            System.err.println("traitement partiel de l'exception " + e.getMessage());
            // throw e; // Relance d'exception
            throw new EmployeException("l'age est négatif", e); // relance d'une exception diférente
        }
        System.out.println("Fin Traitement Employe");
    }

    // throws ->Propager l'exception à la méthode appelante
    public static void traitementAge(int age) throws AgeNegatifException {
        System.out.println("Début Traitement age");
        if (age < 0) {
            throw new AgeNegatifException(age); // throw-> déclencher une exception
        }
        System.out.println("Fin Traitement age");
    }

}
