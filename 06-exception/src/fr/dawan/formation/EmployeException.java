package fr.dawan.formation;

public class EmployeException extends Exception {

    private static final long serialVersionUID = 1L;

    public EmployeException() {
        super();
    }

    public EmployeException(String message, Throwable cause) {
        super(message, cause);

    }

    public EmployeException(String message) {
        super(message);

    }

}
